package com.goods.portal.service;

import com.goods.pojo.TbUser;

public interface UserService {
	TbUser getUserByToken(String token);
}

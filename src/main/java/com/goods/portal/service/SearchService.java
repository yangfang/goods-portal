package com.goods.portal.service;

import com.goods.portal.pojo.SearchResult;

public interface SearchService {
	SearchResult search(String q, int page);
}

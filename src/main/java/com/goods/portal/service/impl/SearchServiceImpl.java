package com.goods.portal.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.goods.common.utils.GoodsResult;
import com.goods.common.utils.HttpClientUtil;
import com.goods.portal.pojo.SearchResult;
import com.goods.portal.service.SearchService;

@Service
public class SearchServiceImpl implements SearchService {

	@Value("${SEARCH_BASE_URL}")
	private String SEARCH_BASE_URL;

	@Override
	public SearchResult search(String q, int page) {
		// 调用goods-search的服务
		Map<String, String> params = new HashMap<>();
		params.put("q", q);
		params.put("page", page + "");

		try {
			// 调用服务
			String json = HttpClientUtil.doGet(SEARCH_BASE_URL, params);
			// 把字符串转换成java对象
			GoodsResult taotaoResult = GoodsResult.formatToPojo(json, SearchResult.class);
			if (taotaoResult.getStatus() == 200) {
				SearchResult result = (SearchResult) taotaoResult.getData();
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}

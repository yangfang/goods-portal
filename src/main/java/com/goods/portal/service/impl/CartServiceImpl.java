package com.goods.portal.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.goods.common.utils.CookieUtils;
import com.goods.common.utils.GoodsResult;
import com.goods.common.utils.HttpClientUtil;
import com.goods.common.utils.JsonUtils;
import com.goods.pojo.TbItem;
import com.goods.portal.pojo.CartItem;
import com.goods.portal.service.CartService;

@Service
public class CartServiceImpl implements CartService {

	@Value("${REST_BASE_URL}")
	private String REST_BASE_URL;
	@Value("${ITEM_INFO_URL}")
	private String ITEM_INFO_URL;

	@Override
	public GoodsResult addCartItem(long itemId, int num, HttpServletRequest request, HttpServletResponse response) {
		CartItem cartItem = null;
		// 获取购物车列表
		List<CartItem> itemList = getCartItemList(request);
		// 判断购物车商品列表中是否存在该商品
		for (CartItem citem : itemList) {
			// 如果存在此商品
			if (citem.getId() == itemId) {
				// 增加数量
				citem.setNum(citem.getNum() + num);
				cartItem = citem;
				break;
			}
		}
		if (cartItem == null) {
			cartItem = new CartItem();
			String json = HttpClientUtil.doGet(REST_BASE_URL + ITEM_INFO_URL + itemId);
			GoodsResult result = GoodsResult.formatToPojo(json, TbItem.class);
			if (result.getStatus() == 200) {
				TbItem item = (TbItem) result.getData();
				cartItem.setId(item.getId());
				cartItem.setTitle(item.getTitle());
				cartItem.setPrice(item.getPrice());
				cartItem.setImage(item.getImage() == null ? "" : item.getImage().split(",")[0]);
				cartItem.setNum(num);
			}
			itemList.add(cartItem);
		}
		// 把购物车列表写入cookie
		CookieUtils.setCookie(request, response, "GOODS_CART", JsonUtils.objectToJson(itemList), true);
		return GoodsResult.ok();
	}

	// 从cookie中取商品列表
	private List<CartItem> getCartItemList(HttpServletRequest request) {
		String cartJson = CookieUtils.getCookieValue(request, "GOODS_CART", true);
		if (cartJson == null) {
			return new ArrayList<>();
		}
		// 把json转换成商品列表
		try {
			List<CartItem> list = JsonUtils.jsonToList(cartJson, CartItem.class);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<>();

	}

	@Override
	public List<CartItem> getCartItemList(HttpServletRequest request, HttpServletResponse response) {
		List<CartItem> itemList = getCartItemList(request);
		return itemList;
	}

	@Override
	public GoodsResult deleteCartItem(long itemId, HttpServletRequest request, HttpServletResponse response) {
		// 从cookie中取购物车商品列表
		List<CartItem> itemList = getCartItemList(request);
		// 从列表中找到此商品
		for (CartItem cartItem : itemList) {
			if (cartItem.getId() == itemId) {
				itemList.remove(cartItem);
				break;
			}

		}
		// 把购物车列表重新写入cookie
		CookieUtils.setCookie(request, response, "GOODS_CART", JsonUtils.objectToJson(itemList), true);

		return GoodsResult.ok();

	}

	@Override
	public GoodsResult updateCartItemNum(long itemId, int num, HttpServletRequest request,
			HttpServletResponse response) {
		CartItem cartItem = null;
		// 获取购物车列表
		List<CartItem> itemList = getCartItemList(request);
		for (CartItem citem : itemList) {
			// 如果存在此商品
			if (citem.getId() == itemId) {
				// 增加数量
				citem.setNum(num);
				cartItem = citem;
				break;
			}
		}
		// 把购物车列表写入cookie
		CookieUtils.setCookie(request, response, "GOODS_CART", JsonUtils.objectToJson(itemList), true);
		return GoodsResult.ok();
	}

}

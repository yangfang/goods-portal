package com.goods.portal.service;

import com.goods.portal.pojo.Order;

public interface OrderService {
	String createOrder(Order order);
}

package com.goods.portal.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.goods.common.utils.GoodsResult;
import com.goods.portal.pojo.CartItem;

public interface CartService {
	GoodsResult addCartItem(long itemId, int num, HttpServletRequest request, HttpServletResponse response);

	List<CartItem> getCartItemList(HttpServletRequest request, HttpServletResponse response);

	GoodsResult deleteCartItem(long itemId, HttpServletRequest request, HttpServletResponse response);

	GoodsResult updateCartItemNum(long itemId, int num, HttpServletRequest request, HttpServletResponse response);
}

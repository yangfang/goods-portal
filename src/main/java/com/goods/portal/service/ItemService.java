package com.goods.portal.service;

import com.goods.portal.pojo.ItemInfo;

public interface ItemService {
	ItemInfo getItemById(long itemId);

	String getItemDescByItemId(long itemId);

	String getItemParam(Long itemId);
}
